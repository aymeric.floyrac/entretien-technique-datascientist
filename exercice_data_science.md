# Exercice data science

L'objectif de cet exercice est d'évaluer vos capacités en machine learning et plus particulièrement dans le domaine du NLP.   

Vous trouverez dans le dossier data2 des données textuelles, ainsi que leurs annotations. L'objectif de l'exercice est de créer un modèle de reconnaissance d'entités nommées (NER), basé sur une sous-partie du [corpus QUAERO](https://quaerofrenchmed.limsi.fr/), permettant de repérer les pathologies dans un texte libre.   
Plus précisément, vous trouverez : 
- un fichier csv __annotations_medline_DISO.csv__ contenant les annotations,   
- un dossier __corpus__ contenant les fichiers txt à utiliser pour entraîner et évaluer le modèle.

Nous attendons de vous que vous produisiez un ou plusieurs notebooks et/ou scripts explicitant votre démarche d'entraînement et d'évaluation du modèle. Vous ne serez pas évalué sur les performances du modèle, mais nous vous encourageons à expliciter vos choix méthodologiques. Vous pouvez utiliser les librairies de votre choix.   
Si vous pensez à une autre méthode que l'entraînement d'un modèle de machine learning pour résoudre  le problème, n'hésitez pas à le mentionner dans votre notebook, sans nécessairement implémenter la méthode.

> Si vous avez besoin d'un GPU et que vous n'en n'avez pas à disposition, n'hésitez pas à utiliser Google Colab, ou bien à écrire votre code comme si vous aviez un GPU mais sans l'exécuter. 

## Références
*Névéol A, Cohen, KB, Grouin C, Hamon T, Lavergne T, Kelly L, Goeuriot L, Rey G, Robert A, Tannier X, Zweigenbaum P. Clinical Information Extraction at the CLEF eHealth Evaluation lab 2016. CLEF 2016, Online Working Notes, CEUR-WS 1609.2016:28-42.*